### Alexander Blanchette
# LIS 3781 - Assignment 4 Requirements:
  - Develop a database for a Office Supply Company that tracks day-to-day business operations.
  - The CFO needs an updated method of storing data, running reports, and making business decisions based upon trends and forecasts.
  - Being able to track historical data is important due to new governmental regulations. 
  - Assignment 4 will be related to assignment 5

![Assignment 4 Overview](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/039d793c17336ced68209772507964ef9e6b19b5/a4/img/a4deliverables.JPG)

## ERD For Assignment 4

![Assignment 4 ERD](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/039d793c17336ced68209772507964ef9e6b19b5/a4/img/A4ERDFinal.JPG)

> Below you will find a .txt file containing all of the SQL script for A4.
>

>[A4 SQL Script](a4/sqlCodeAndModels/A4SQLscript.sql)
