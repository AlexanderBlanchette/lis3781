> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Assignment 1

## Welcome! Feel free to have a look around.

#

> ### Assignment 1 Requirements:
    - Install AMPPS
    - Provide screenshots of install locations
    - Create Bitbucket repo
    - Compile Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions.


> #### A1 Database Rules:
The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes: job description, length of employment, benefits, number of dependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories must be tracked. Also, include the following Business Rules:

- Each employee may have one or more dependents.
- Each employee has only one job.
- Each job can be held by many employees.
- Many employees may receive many benefits.
- Many benefits may be selected by many employees (though, while they may not select any benefits—
any dependents of employees may be on an employee’s plan).
- Employee/Dependent tables must use suitable attributes (see assignment guidelines):
- 
In Addition:
- Employee: SSN, DOB, start/end dates, salary;
- Dependent: same infrmation as their associated employee (though, not start/end dates), date added
  (as dependent), type of relationship: e.g., father, mother, etc.
- Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)
- Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.)
- Plan: type (single, spouse, family), cost, election date (plans must be unique)
- Employee history: jobs, salaries, and benefit changes, as well as who made the change and why;
- Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ);
- *All* tables must include notes attribute.


### README.md file should include the following items:
- Screenshot of A1 ERD
- Ex1. SQL Solution
- Git commands (see below)

> #### Git commands w/short descriptions:
1. git init - Crate an empty Git repository or reinitialize and existing one.
2. git status - Show working tree status.
3. git add - Add file contents to the index.
4. git commit - Record changes to the repository.
5. git push - Update remote refs along with associated objects.
6. git pull - Grab data from remote reository and integrate it with another repository or local branch.
7. One additional git command (your choice) - git Merge - Used to integrate different brances into a single branch.


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

<<<<<<< HEAD
![AMPPS Installation Screenshot](img/ampps.png)

#### Screenshot of A1 ERD

![A1 ERD Screenshot](img/a1ERD.png)
=======
![AMPPS Installation Screenshot](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/1610afcb9ad66d181cd334637b9ce5ee799b65ff/a1/img/ampps.PNG)

#### Screenshot of A1 ERD

![A1 ERD Screenshot](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/1610afcb9ad66d181cd334637b9ce5ee799b65ff/a1/img/a1ERD.PNG)
>>>>>>> d0df307bfd01c0d4dedcc4929125bf71c133162f

#### Screenshot of A1 Ex1

![A1 Ex1 Screenshot](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/a5a126074a563100b396301ea3e4e2648793b966/a1/img/Ex1.PNG)


#### Tutorial Links.

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/AlexanderBlanchette/bitbucketstationlocation "Bitbucket Station Locations")
