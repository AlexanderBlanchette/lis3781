DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
(
cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
cmp_id INT UNSIGNED NOT NULL, cus_ssn binary(64) not null,
cus_standing binary(64) not null COMMENT '*only* demo purposes - do *NOT* use *SALT* in deployment!',
cus_type enum('Loyal', 'Discount','Impulse','Need-Based','Wandering'),
cus_first VARCHAR(15) NOT NULL, cus_last VARCHAR(30) NOT NULL,
cus_street VARCHAR(30) NULL, cus_city VARCHAR(30) NULL, cus_state CHAR(2) NULL, cus_zip int(9) unsigned ZEROFILL NULL,
cus_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, not US area codes',
cus_email VARCHAR(100) NULL,
cus_balance DECIMAL(7,2) unsigned NULL COMMENT '11,333.67',
cus_tot_sales DECIMAL(7,2) unsigned NULL,
 cus_notes VARCHAR(255) NULL,
PRIMARY KEY (cus_id), UNIQUE INDEX ux_cus_ssn (cus_ssn ASC), INDEX idx_cmp_id (cmp_id ASC),
-- Comment CONSTRAINT line to demo DBMS auto value when *not* using "constraint" option for foreign keys, then... SHOW CREATE TABLE customer; 
CONSTRAINT fk_customer_company
	FOREIGN KEY (cmp_id)
	REFERENCES company (cmp_id)
	ON DELETE NO ACTION
	ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;
SHOW WARNINGS;
-- salting and hashing sensitive data (e.g., SSN). Normally, *each* record would receive unique random salt!
select * from customer;
set @cus_standing=RANDOM_BYTES(64);
INSERT INTO customer
VALUES
(null,2,unhex(SHA2(CONCAT(@cus_standing, 000456789),512)),@cus_standing,'Discount','Erica', 'Perez','Track Trail','Baleview','MO','33421','2436678732', 'Erica@gmail.com','12333.17','20134.23','customer notes1'),
(null,4,unhex(SHA2(CONCAT(@cus_standing, 001456789),512)),@cus_standing,'Wandering','Ali','Anne','Drift Rd.','Nipton','MA','88765	','4453856673', 'ali@aol.com','456.57','2125.70','customer notes2'),
(null,3,unhex(SHA2(CONCAT(@cus_standing, 002456789),512)),@cus_standing,'Need-Based','David', 'Thompson','Walter Rd.','Sharksville	','CA','33567	','3356726432','test3@mymail.com','8730.23','92678.00','customer notes3'),
(null,5,unhex(SHA2(CONCAT(@cus_standing, 003456789),512)),@cus_standing,'Impulse','Justin','Peters','915 Drive Past','Penwell','TX','009135674','2145558122', 'David@aol.com','26521.19','48325.00','customer notes4'),
(null,1,unhex(SHA2(CONCAT(@cus_standing, 004456789),512)),@cus_standing,'Loyal','Brett','Jackson','Rocky Rd.','Boulderville','AZ','23245','5546874388', 'Brett@aol.com','4565.73','8121.00','customer notes5');
SHOW WARNINGS;
 -- set foreign_key_checks=1;
select * from company;
select * from customer;