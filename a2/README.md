
# LIS 3781

## Alexander Blanchette

### Assignment 2 Requirements:

![Screenshot of page 1 deliverables](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/a97d1b5b33fbb19eb6b054b85a2c3c2593f52026/a2/img/a2reqs1.PNG)
![Screenshot of page 2 deliverables](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/a97d1b5b33fbb19eb6b054b85a2c3c2593f52026/a2/img/a2reqs2.PNG)

#### Assignment Deliverables:

*Screenshot of Customer Table SQL Script:

![SQL Script to generate customer table](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/a97d1b5b33fbb19eb6b054b85a2c3c2593f52026/a2/img/customertablesqlscript.PNG)
> ##### Screenshot of the sql script that crated the customer table below:
>
> *Screenshot of Customer Table:

![Image of customer table](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/a97d1b5b33fbb19eb6b054b85a2c3c2593f52026/a2/img/customertable.PNG)

*Screenshot of Company Table SQL Script:

![SQL Script to generate company table](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/a97d1b5b33fbb19eb6b054b85a2c3c2593f52026/a2/img/companytablesqlscript.PNG)
>##### Screenshot of the sql script that crated the company table below:
>
>*Screenshot of Company Table:

![Image of customer table](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/a97d1b5b33fbb19eb6b054b85a2c3c2593f52026/a2/img/companytable.PNG)