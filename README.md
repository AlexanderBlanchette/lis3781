> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 - Advanced Database Management

## Alexander Blanchette

### LIS 3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/ "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of install locations
    - Create Bitbucket repo [Bitbucket Station Location Tutorial](LINK/README.md "Bitbucket tutorial README.md file")
    - Compile Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions.

2. [A2 README.md](a2/ "My A2 README.md file")
    - A2 Bitbucket requirements will be put w/in A1
    - Use SQL to create tables and insert data
    - use SQL to create users and delagate certain privliges (following the principle of least access)

3. [A3 README.md](a3/ "My A3 README.md file")
    - Learn how to use Oracle's DBMS system
    - Create, Deploy, and Insert data
    - Learn about specific "Oracaleisms"
4. [A4 README.md](a4/ "My A4 README.md file")
    - Develop a database for a Office Supply Company that tracks day-to-day business operations.
    - The CFO needs an updated method of storing data, running reports, and making business decisions based upon trends and forecasts.
    - Being able to track historical data is important due to new governmental regulations. 
    - Assignment 4 will be related to assignment 5
5. [A5 README.md](a5/ "My A5 README.md file")
  - Expanding upon the high-volume home office supply company's data tracking requirements, the CFO requests your services again to extend the data models functionality
  - The CFO needs to you to create a small data mart to test the capabilities of data warehousing analytics and business intelligence.
  - Board members also want this expansion to track data on store fronts across the nation:
    - Add the following tables 
      - Region
        - State
        - City
        - Store. 
  - See A5 Deliverables for more rules and constraints that must be met for this assignment.
6. [P1 README.md](p1/ "My P1 README.md file")
    - Design a database that keeps track of the cities cour case data.
    - Designed triggers to fire when certain requirements are met to increase automation.
7. [P2 README.md](p2/ "My P2 README.md file")
    - The goal of this project is to become famililar with NoSQL enviroments, specifically MongoDB.
    - This project consists of multiple questions designed to get students familiar with JSON and Mongodb's functions