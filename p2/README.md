### Alexander Blanchette
# LIS 3781 - Project 2 Requirements:
  - The goal of this project is to become famililar with NoSQL enviroments, specifically MongoDB.
    - This project consists of multiple questions designed to get students familiar with JSON and Mongodb's functions

  - See P2 Deliverables for more rules and constraints that must be met for this assignment.

![Project 2 Overview](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/708a58a7a95af87398c6d88119530473439a9155/p2/img/Deliverables.PNG)

## Project 2 Questions 

![P2 Questions](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/708a58a7a95af87398c6d88119530473439a9155/p2/img/p2questions.PNG)
##### Below you will find screenshots of mongodb running in windows cmd console.
> This is mongo.exe running in Windows Command Prompt  
> ![P2 mongo console](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/708a58a7a95af87398c6d88119530473439a9155/p2/img/mongoconsole.PNG)
> 
> This is mongod.exe running a local server in Windows Command Prompt  
> ![P2 mongod console](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/708a58a7a95af87398c6d88119530473439a9155/p2/img/mongodconsole.PNG)
> Below you will find a .txt file containing answers for P2.
>[P2 Answers](https://bitbucket.org/AlexanderBlanchette/lis3781/src/master/p2/sqlCodeAndModels/Mongodbanswers.sql)