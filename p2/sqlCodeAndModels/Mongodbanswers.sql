1. db.restraunts.findOne()

2. db.restaurants.find().count();
db.restaurants.find()[25358];
3.db.restaurants.find().limit(5);
4.db.restaurants.find() {"borough": "Brooklyn"}}.count() --dont need count function for answer
5. db.restaurants.find({"cuisine": "American"}):
db.restaurants.find({"cuisine":"American "}) -- they had an error
db.restaurants.find({"cuisine":/^American\s$/i }) -- allows for whitespace after n as well as case inscensitive characters.
6. db.restaurants.find({"borough": "Manhatten","cuisine": "Hamburgers"})
7. db.restaurants.find({"borough": "Manhatten","cuisine": "Hamburgers"}).count();
8. db.restaurants.find({"address.zipcode": "10075"}).count()
9.db.restaurants.find({"cuisine": "Chicken","adress.zipcode": "10024"})// implicit AND operation
10. db.restaurants.find({ $or: [ {"cuisine": "Chicken"},{"adress.zipcode": "10024"}]})// implicit OR operation
11. db.restaurants.find({"borough": "Queens", "cuisine": "Jewish/Kosher"}).sort({"address.zipcode": -1}).count() -- note: capital j and k 
12. db.restaurants.find({"grades.grade":"A"})
13. db.restaurants.find({"grades.grade":"A"}, {"name":1, "grades.grade":1})
14. db.restaurants.find({"grades.grade":"A"}, {"name":1, "grades.grade":1, _id:0})
15. db.restaurants.find({"grades.grade":"A"}sort({"cuisine":1, "address.zipcode":-1}) --46 39
16. db.restaurants.find({"grades.score":{$gt:80}})
17. -- 49:26
db.restaurants.insert(
{
        "address" : {
           "building" : "1000",
           "coord" : [
             -58.9557413, 31.7720266
             ],
             "street" : "7th Ave",
             "zipcode" : "10024"
        },
        "borough" : "Brooklyn",
        "cuisine" : "BBQ",
        "grades" : [
                {
                        "date" : ISODate("2015-11-05T00:00:00Z"),
                        "grade" : "C",
                        "score" : 15
                },
               ],
        "name" : "Big Tex",
        "restaurant_id" : "61704627"
}
)


18. --1:05:55
db.restaurants.update(
{"_id" : ObjectId("Se9ca93998816875e84c47ca") 
},
{
$set: { “cuisine”: "Steak and Sea Food" }, $currentDate: { "lastModified": true }
}
)

19. db.restaurants.remove({"name": "White Castle"})
20. 
db.restaurants.