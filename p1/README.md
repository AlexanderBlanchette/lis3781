### Alexander Blanchette
# LIS 3781 - Project 1 Requirements:


![AMPPS Installation Screenshot](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/db82ceea38eeaa3bb497c973c83d8f7fd1771177/p1/img/p1deliverables.JPG)

## ERD For Project 1

![Project 1 ERD](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/db82ceea38eeaa3bb497c973c83d8f7fd1771177/p1/img/p1erd.JPG)

> Below you will find a .txt file containing all of the SQL script for this project, Inculding questions 1 - 6.
>

>[P1 SQL Script](sqlCodeAndModels/p1sqlscript.txt)
>
> Below you will find a .txt file giving solutions for Questions 1 - 6.
>          
> [P1 Solutions](sqlCodeAndModels/p1thru6.txt)
> ![AMPPS Installation Screenshot](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/db82ceea38eeaa3bb497c973c83d8f7fd1771177/p1/img/q1thru6.JPG)
> 