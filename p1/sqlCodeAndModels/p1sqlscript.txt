DROP SCHEMA IF EXISTS ahb16e;
CREATE SCHEMA IF NOT EXISTS ahb16e DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE ahb16e;
-- line 33 change salt
-- Table person
-- NOTE: allow per_ssn and per_salt to be null, in order to use stored proc CreatePersonSSN below
DROP TABLE IF EXISTS person;
CREATE TABLE IF NOT EXISTS person
(
per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
per_ssn BINARY(64) NULL,
per_salt binary(64) null, --  '*only* for demo purposes - *REMEMBER* to change salt!'
per_fname VARCHAR(15) NOT NULL,
per_lname VARCHAR(30) NOT NULL,
per_street VARCHAR(30) NOT NULL,
per_city VARCHAR(30) NOT NULL,
per_state CHAR(2) NOT NULL,
per_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
per_email VARCHAR(100) NOT NULL,
per_dob DATE NOT NULL,
per_type ENUM('a','c','j') NOT NULL,
per_notes VARCHAR(255) NULL,
PRIMARY KEY (per_id),
UNIQUE INDEX ux_per_ssn (per_ssn ASC)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE utf8_unicode_ci;
SHOW WARNINGS;
-- Data Person Table
START TRANSACTION;
INSERT INTO person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve', 'Rogers', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', '1923-10-03', 'C', NULL),
(NULL, NULL, NULL, 'Bruce', 'Wayne', '1007 Mountain Drive', 'Gotham', 'NY', 003208440, 'bwayne@knology.net', '1968-03-20', 'c', NULL),
(NULL, NULL, NULL, 'Peter', 'Parker', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', '1988-09-12', 'c', NULL),
(NULL, NULL, NULL, 'Jane', 'Thompson', '13563 Ocean View Drive', 'Seattle', 'WA', 032084409, 'jthompson@gmail.com', '1978-05-08', 'c', NULL),
(NULL, NULL, NULL, 'Debra', 'Steele', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', '1994-07-19', 'c', NULL),
(NULL, NULL, NULL, 'Tony', 'Stark', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', '1972-05-04', 'a', NULL),
(NULL, NULL, NULL, 'Hank', 'Pymi', '2355 Brown Street', 'Cleveland', 'OH', 022348890, 'hpym@aol.com', '1980-08-28', 'a', NULL),
(NULL, NULL, NULL, 'Bob', 'Best', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', '1992-02-10', 'a', NULL),
(NULL, NULL, NULL, 'Sandra', 'Dole', '87912 Lawrence Ave', 'Atlanta', 'GA', 002348890, 'sdole@gmail.com', '1990-01-26', 'a', NULL),
(NULL, NULL, NULL, 'Ben', 'Avery', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', '1983-12-24', 'a', NULL),
(NULL, NULL, NULL, 'Arthur', 'Curry', '3304 Euclid Avenue', 'Miami', 'FL', 000219932, 'acurry@gmail.com', '1975-12-15', 'j', NULL),
(NULL, NULL, NULL, 'Diana', 'Price', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dprice@symaptico.com', '1980-08-22', 'j', NULL),
(NULL, NULL, NULL, 'Adam', 'Jurris', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', '1995-01-31', 'j', NULL),
(NULL, NULL, NULL, 'Judy', 'Sleen', '56343 Rover Ct.', 'Billings', 'MT', 672048823, 'jsleen@symaptico.com', '1970-03-22', 'j', NULL),
(NULL, NULL, NULL, 'Bill', 'Neiderheim', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', '1982-03-13', 'j', NULL);
COMMIT;

-- salt hash
-- Populate person table with hashed and salted SSN numbers. *MUST* include salted value in DB!
DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN
DECLARE X, Y INT;
SET X = 1;
-- dynamically set loop ending value (total # persons)
select count(*) into y from person;
-- select y; -- display # of persons (only for testing)
WHILE x <= y DO
-- give each person a unique randomized salt, and hashed and salted randomized SSN.
-- Note: 'salt is used for demo puposes - *ALWAYS* change it!
SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user when looping
SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; -- random 9-digit SSN from 111111111 - 999999999, inclusive (see link below)
SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512)); -- each user's SSN value is uniquely randomized and uniquely salted!
-- RAND([N]): Returns random floating-point value v in the range 0 <= v < 1.0
-- randomize ssn between 111111111 - 999999999 (note: using value 000000000 for ex. 4 below)
update person
set per_ssn=@ssn, per_salt=@salt
where per_id=x;
SET X = x + 1;
END WHILE;
END$$
DELIMITER ;
call CreatePersonSSN();

-- attorney table
DROP TABLE IF EXISTS attorney; 
CREATE TABLE IF NOT EXISTS attorney
(
per_id SMALLINT UNSIGNED NOT NULL,
aty_start_date DATE NOT NULL,
aty_end_date DATE NULL DEFAULT NULL,
aty_hourly_rate DECIMAL(5,2)  NOT NULL,
aty_years_in_practice TINYINT NOT NULL,
aty_notes VARCHAR(255) NULL DEFAULT NULL,
PRIMARY KEY (per_id), 

INDEX Idx_per_Id (per_Id ASC), 

CONSTRAINT fk_attorney_person
FOREIGN KEY (per_id)
REFERENCES person (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE 
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE= utf8mb4_0900_ai_ci;
SHOW WARNINGS;
-- Insert attorney table
START TRANSACTION;
INSERT INTO attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
VALUES
(6, '2006-06-12', NULL, 85, 5, NULL),
(7, '2003-08-20', NULL, 130, 28, NULL),
(8, '2009-12-12', NULL, 70, 17, NULL),
(9, '2008-06-08', NULL, 78, 13, NULL),
(10, '2011-09-12',NULL, 60, 24, NULL);
COMMIT;

-- client table
DROP TABLE IF EXISTS client;
CREATE TABLE IF NOT EXISTS client
(
per_id SMALLINT UNSIGNED NOT NULL,
Cli_notes VARCHAR(255) NULL DEFAULT NULL,
PRIMARY KEY (per_id),

INDEX (per_id ASC),

CONSTRAINT fk_client_person
FOREIGN KEY (per_id)
REFERENCES person (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE= utf8mb4_0900_ai_ci;

SHOW WARNINGS;
                
-- court table
DROP TABLE IF EXISTS court;
CREATE TABLE IF NOT EXISTS court
(
crt_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
crt_name VARCHAR(45) NOT NULL,
crt_street VARCHAR(30) NOT NULL,
crt_city VARCHAR(30) NOT NULL,
crt_state CHAR(2) NOT NULL,
crt_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
crt_phone BIGINT NOT NULL,
crt_email VARCHAR(100) NOT NULL,
crt_url VARCHAR(100) NOT NULL,
crt_notes VARCHAR(255) NULL,
PRIMARY KEY (crt_id)
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE= utf8mb4_0900_ai_ci;
SHOW WARNINGS;

-- INSERT INTO table court
START TRANSACTION;

INSERT INTO court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
VALUES
(NULL, 'leon county circuit court', '301 south monroe street ', 'tallahassee', 'fl', 323035292, 8506065504, 'iccc@us.fi.gov', 'http: //www.leoncountycircultcourt.gov/', NULL),
(NULL, 'leon country traffic court', '1921 thomasville road', 'tallahassee', 'fl', 323035292, 8505774100, 'Ictc@us.fi.gov', 'http://www _.leoncountytrafficcourt.gov/', NULL),
(NULL, 'florida supreme court', '500 south duvaletreet', 'tallahassee', 'fl', 323035292, 8504880125, 'fse@us.fi.gov', 'http://www.floridasupremecourt.org/', NULL),
(NULL, 'orange country courthouse', '424 north orange avenue', 'orlando', 'fl', 328012248, 4078362000, 'occ@us.fi.gov', 'http://www.ninthcircult.org/', NULL),
(NULL, 'fifth district court of appeal', '300 south beach street', 'daytona beach', 'fl', 321158763, 3862258600, 'Sdca@us.fl.gov', 'http://www.5dca.org/', NULL);
COMMIT;

-- Data for table phone

-- Data for table client (also, could include other client attributes)
START TRANSACTION;
INSERT INTO client
(per_id, cli_notes)
VALUES
(1, NULL),
(2, NULL),
(3, NULL),
(4, NULL),
(5, NULL);
COMMIT;



-- Judge Table

DROP TABLE IF EXISTS judge;
CREATE TABLE IF NOT EXISTS judge
(
per_id SMALLINT UNSIGNED NOT NULL,
crt_id TINYINT UNSIGNED NULL DEFAULT NULL,
jud_salary DECIMAL(8,2) NOT NULL,
jud_years_in_practice TINYINT UNSIGNED NOT NULL,
jud_notes VARCHAR(255) NULL DEFAULT NULL,
PRIMARY KEY (per_id),

INDEX idx_per_id (per_id ASC),
INDEX idx_crt_id (crt_id ASC),

CONSTRAINT fk_judge_person
FOREIGN KEY (per_id)
REFERENCES person (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE,

CONSTRAINT fk_judge_court
FOREIGN KEY (crt_id)
REFERENCES court (crt_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8mb4
COLLATE= utf8mb4_0900_ai_ci;
SHOW WARNINGS;
-- 
-- INSERT INTO table judge
START TRANSACTION;

INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES
(11, 5, 150000, 10, NULL),
(12, 4, 185000, 3, NULL),
(13, 4, 135000, 2, NULL),
(14, 3, 170000, 6, NULL),
(15, 1, 120000, 1, NULL);
COMMIT;

-- judge_hst table
DROP TABLE IF EXISTS judge_hist;
CREATE TABLE IF NOT EXISTS judge_hist
(
jhs_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
per_id SMALLINT UNSIGNED NOT NULL,
jhs_crt_id TINYINT NULL,
jhs_date timestamp NOT NULL default current_timestamp(),
jhs_type enum('i', 'u', 'd') NOT NULL default 'i',
jhs_salary DECIMAL(8,2) NOT NULL,
jhs_notes VARCHAR(255) NULL,
PRIMARY KEY (jhs_id),

INDEX idx_per_id (per_id ASC),

CONSTRAINT fk_judge_hist_judge
FOREIGN KEY (per_id)
REFERENCES judge (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE= utf8mb4_0900_ai_ci;
SHOW WARNINGS;

-- INSERT INTO judge hist
START TRANSACTION;
INSERT INTO judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES
(NULL, 11, 3, '2009-01-16', 'i', 130000, NULL),
(NULL, 12, 2, '2010-05-27', 'i', 140000, NULL),
(NULL, 13, 5, '2000-01-02', 'i', 115000, NULL),
(NULL, 13, 4, '2005-07-05', 'i', 135000, NULL),
(NULL, 14, 4, '2008-12-09', 'i', 155000, NULL),
(NULL, 15, 1, '2011-03-17', 'i', 120000, 'freshman justice'),
(NULL, 11, 5, '2010-07-05', 'i', 150000, 'assigned to another court'),
(NULL, 12, 4, '2012-10-08', 'i', 165000, 'became chief justice'),
(NULL, 14, 3, '2009-04-19', 'i', 170000, 'reassigned to court based upon local area population growth');
COMMIT;

-- case table
-- Table 'case' 21:46
DROP TABLE IF EXISTS `case`;
CREATE TABLE IF NOT EXISTS `case`
(
cse_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
per_id SMALLINT UNSIGNED NOT NULL,
cse_type VARCHAR(45) NOT NULL,
cse_description TEXT NOT NULL,
cse_start_date DATE NOT NULL,
cse_end_date DATE NULL,
cse_notes VARCHAR(255) NULL,
PRIMARY KEY (cse_id),
INDEX idx_per_id (per_id ASC),

CONSTRAINT fk_court_case_judge
FOREIGN KEY (per_id)
REFERENCES judge (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE= utf8mb4_0900_ai_ci;
SHOW WARNINGS;


-- INSERT INTO table case
START TRANSACTION;

INSERT INTO `case` -- 41:33
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
VALUES
(NULL, 13, 'civil', 'client says that his logo Is being used without his consent to promote a rival business', '2010-09-09', NULL, 'copyright infringement'),
(NULL, 12, 'criminal', 'client Is charged with assaulting her husband during an argument', '2009-11-18', '2010-12-23', 'assault'),
(NULL, 14, 'civil', 'client broke an ankle while shopping at a local grocery store. no wet floor sign was posted although the floor had just been mopped', '2008-05-06', '2008-07-23', 'slip and fall'),
(NULL, 11, 'criminal', 'client was charged with stealing several televisions from his former place of employment. the client has a solid alibi', '2011-05-20', NULL, 'grand theft'),
(NULL, 13, 'criminal', 'client charged with possession of 10 grams of cocaine, allegedly found In his glove box by state police', '2011-06-05', NULL, 'possession of narcotics'),
(NULL, 14, 'civil', 'client alleges newspaper printed false information about his personal activities while he ran a large laundry business In a small nearby town.', '2007-01-19', '2007-05-20', 'defamation'),
(NULL, 12, 'criminal', 'client charged with the murder of his co-worker over a lovers feud. the client has no alibi', '2010-03-20', NULL, 'murder'),
(NULL, 15, 'civil', 'client made the horrible mistake of selecting a degree other than IT and had to declare bankruptcy.', '2012-01-26', '2013-02-28', 'bankruptcy');

COMMIT;

-- Table bar 22:03
DROP TABLE IF EXISTS bar;
CREATE TABLE IF NOT EXISTS bar
(
Bar_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
per_id SMALLINT UNSIGNED NOT NULL,
bar_name VARCHAR(45) NOT NULL,
bar_notes VARCHAR(255) NULL,
PRIMARY KEY (bar_id),
INDEX idx_per_id (per_id ASC),

CONSTRAINT fk_bar_attomey
FOREIGN KEY (per_id)
REFERENCES attorney (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE= utf8mb4_0900_ai_ci;
SHOW WARNINGS;
-- insert table bar
START TRANSACTION;

INSERT INTO bar
(bar_id, per_id, bar_name, bar_notes)
VALUES
(NULL, 6, 'Florida bar', NULL),
(NULL, 7, 'Alabama bar', NULL),
(NULL, 8, 'Georgia bar', NULL),
(NULL, 9, 'Michigan bar', NULL),
(NULL, 10, 'South Carolina bar', NULL),
(NULL, 6, 'Montana bar', NULL),
(NULL, 7, 'Arizona Bar', NULL),
(NULL, 8, 'Nevada Sar', NULL),
(NULL, 9, 'New York Bar', NULL),
(NULL, 10, 'New York Bar', NULL),
(NULL, 6, 'Mississippi Bar', NULL),
(NULL, 7, 'California Bar', NULL),
(NULL, 8, 'Illinois Bar', NULL),
(NULL, 9, 'Indiana Bar', NULL),
(NULL, 10, 'Illinois Bar', NULL),
(NULL, 6, 'Tallahassee Bar', NULL),
(NULL, 7, 'Ocala Bar', NULL),
(NULL, 8, 'Bay County Bar', NULL),
(NULL, 9, 'Cincinatti Bar', NULL);
COMMIT;
-- Table specialty
DROP TABLE IF EXISTS specialty;
CREATE TABLE IF NOT EXISTS specialty
(
spc_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
per_id SMALLINT UNSIGNED NOT NULL,
spc_type VARCHAR(45) NOT NULL,
spc_notes VARCHAR(255) NULL,
PRIMARY KEY (spc_id),
INDEX idx_per_id (per_id ASC),

CONSTRAINT fk_specialty_attorney
FOREIGN KEY (per_id)
REFERENCES attorney (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE= utf8mb4_0900_ai_ci;
SHOW WARNINGS;

-- INSERT INTO specialty
START TRANSACTION;
INSERT INTO specialty
(spc_id, per_id, spc_type, spc_notes)
VALUES
(NULL, 6, 'business', NULL),
(NULL, 7, 'traffic', NULL),
(NULL, 8, 'bankruptcy', NULL),
(NULL, 9, 'insurance', NULL),
(NULL, 10, 'judicial', NULL),
(NULL, 6, 'environmental', NULL),
(NULL, 7, 'criminal', NULL),
(NULL, 8, 'real estate', NULL),
(NULL, 9, 'malpractice', NULL);
COMMIT;

-- assignment table
CREATE TABLE IF NOT EXISTS assignment
(
asn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
per_cid SMALLINT UNSIGNED NOT NULL,
per_aid SMALLINT UNSIGNED NOT NULL,
cse_id SMALLINT UNSIGNED NOT NULL,
asn_notes VARCHAR(255) NULL,
PRIMARY KEY (asn_id),

INDEX idx_per_cid (per_cid ASC),
INDEX idx_per_aid (per_aid ASC),
INDEX idx_cse_id (cse_id ASC),

UNIQUE INDEX ux_per_cid_per_aid_cse_id (per_cid ASC, per_aid ASC, cse_id ASC),

CONSTRAINT fk_assign_case
FOREIGN KEY (cse_Id)
REFERENCES `case` (cse_id)
ON DELETE NO ACTION
ON UPDATE CASCADE,

CONSTRAINT fk_assignment_client
FOREIGN KEY (per_cid)
REFERENCES client (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE,

CONSTRAINT fk_assignment_attorney
FOREIGN KEY (per_aid)
REFERENCES attorney (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE= utf8mb4_0900_ai_ci;
SHOW WARNINGS;

-- Data for table assignment
START TRANSACTION;
INSERT INTO assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
VALUES
(NULL, 1, 6, 7, NULL),
(NULL, 2, 6, 6, NULL),
(NULL, 3, 7, 2, NULL),
(NULL, 4, 8, 2, NULL),
(NULL, 5, 9, 5, NULL), -- S
(NULL, 1, 10, 1, NULL),
(NULL, 2, 6, 3, NULL),
(NULL, 3, 7, 8, NULL),
(NULL, 4, 8, 8, NULL),
(NULL, 5, 9, 8, NULL),
(NULL, 4, 10, 4, NULL);
COMMIT;

-- Table phone
DROP TABLE IF EXISTS phone;
CREATE TABLE IF NOT EXISTS phone
(
phn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
per_id SMALLINT UNSIGNED NOT NULL,
phn_num BIGINT UNSIGNED NOT NULL,
phn_type ENUM('h','c','w','f') NOT NULL, -- 'home, cell, work, fax'
phn_notes VARCHAR(255) NULL,
PRIMARY KEY (phn_id),

INDEX idx_per_id (per_id ASC),

CONSTRAINT fk_phone_person
FOREIGN KEY (per_id)
REFERENCES person (per_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE= utf8mb4_0900_ai_ci;
SHOW WARNINGS;

-- INSERT INTO PHONE
START TRANSACTION;
INSERT INTO phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
VALUES
(NULL, 1, 8032288827, 'c', NULL),
(NULL, 2, 2052338293, 'h', NULL),
(NULL, 4, 1034325598, 'w', 'has two office numbers'),
(NULL, 5, 6402338494, 'w', NULL),
(NULL, 6, 5508329842, 'f', 'fax number not currently working'),
(NULL, 7, 8202052203, 'c', 'prefers home calls'),
(NULL, 8, 4008338294, 'h', NULL),
(NULL, 9, 7654328912, 'w', NULL),
(NULL, 10, 5463721984, 'f', 'work fax number'),
(NULL, 11, 4537821902, 'h', 'prefers cell phone calls'),
(NULL, 12, 7867821902, 'w', 'best number to reach'),
(NULL, 13, 4537821654, 'w', 'call during lunch'),
(NULL, 14, 3721821902, 'c', 'prefers cell phone calls'),
(NULL, 15, 9217821945, 'f', 'use for faxing legal docs');
COMMIT;












-- trigger ex4 not needed, but kept for refrenceing

-- select '4) Create a trigger that automatically adds a record to the judge history table for every record **added** to the judge table.'' as';
-- do sleep(5);
-- Note: technically, a judge could already be in the person table, yet not added to the judge table. Or, here, add a new person.
-- add 16th person (a judge), before adding person to judge table (salt and hash per_ssn, and store unique salt, per_salt)

-- select 'show person data *before* adding person record' ' as ';
-- select per_id, per_fname, per_lname from person;
--  do sleep(5);

-- give person a unique randomized salt, then hash and salt SSN

-- SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user
-- SET @num=000000000; -- Note: already provided random SSN from 111111111 - 999999999, inclusive above
-- SET @ssn=unhex(sha2(concat(@salt, @num), 512)); -- salt and hash person's SSN 000000000

-- INSERT INTO person
-- (per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
values
-- (NULL, @ssn, @salt, 'Bobby', 'Sue', '123 Main St', 'Panama City Beach', 'FL', 324530221, 'bsue@fl.gov', '1962-05-16', 'j', 'new district judge');

-- select 'show person data *after* adding person record' 'as';
-- select per_id, per_fname, per_lname from person;
-- do sleep(5);


-- _________________________________________________________________________________________
-- |                                                                                         |
-- |QUESTIONS 1 THROUGH 6 INCLUDING EXTRA CREDIT                                             |
-- |_________________________________________________________________________________________|

-- 1.

		Drop VIEW if exists v_attorney_info;
		CREATE VIEW v_attorney_info AS

		select
		concat(per_Iname, ", ", per_fname) as name,
		concat(per_street, ", ", per_city, ", ", per_state, "", per_zip) as address,
		TIMESTAMPDIFF(year, per_dob, now()) as age,
		CONCAT('$', FORMAT(aty_hourly_rate, 2)) as hourly_rate,
		bar_name, spc_type
		from person
		natural join attorney
		natural join bar
		natural join specialty
		order by per_Iname;

		select 'display view v_attorney_info' "as";

		select * from v_attorney_info;
		drop VIEW if exists v_attorney_info;


-- 2. 1:29:00 

		drop procedure if exists sp_num_judges_born_by_month;
		DELIMITER //
		CREATE PROCEDURE sp_num_judges_born_by_month()
		BEGIN
		select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
		from person
		natural join judge
		group by month_name
		order by month;
		END //
		DELIMITER ;

		select 'calling sp_num_judges_born_by_month()' "as";

		call sp_num_judges_born_by_month();
		drop procedure if exists sp_num_judges_born_by_month;

-- 3. 1:31:16

		drop procedure if exists sp_cases_and_judges;
		DELIMITER //
		CREATE PROCEDURE sp_cases_and_judges()
		BEGIN
		select per_id, cse_id, cse_type, cse_description,
		concat(per_fname, "", per_Iname) as name,
		concat('(',substring(phn_num, 1, 3), ')', substring(phn_num, 4, 3), '-', substring(phn_num, 7, 4)) as judge_office_num,
		phn_type,
		jud_years_in_practice,
		cse_start_date,
		cse_end_date
		from person
		natural join judge
		natural join `case`
		natural join phone
		where per_type='j'
		order by per_Iname;

		END //
		DELIMITER ;

		select 'sp_cases_and_judges()' "as" ;
		CALL sp_cases_and_judges();
		drop procedure if exists sp_cases_and_judges;
		
-- 4. 1:38:00

		select '4) Create a trigger that automatically adds a record to the judge history table for every record **added** to the judge table.' "as";
		do sleep(5);

		-- Note: technically, a judge could already be in the person table, yet not added to the judge table. Or, here, add a new person.
		-- add 16th person (a judge), before adding person to judge table (salt and hash per_ssn, and store unique salt, per_salt)

		select 'show person data *before* adding person record' "as";
		select per_id, per_fname, per_Iname from person;
		do sleep(5);

		-- give person a unique randomized salt, then hash and salt SSN
		SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user
		SET @num=000000000; -- Note: already provided random SSN from 111111111 - 999999999, inclusive above
		SET @ssn=unhex(sha2(concat(@salt, @num), 512)); -- salt and hash person's SSN 000000000

		INSERT INTO person
		(per_id, per_ssn, per_salt, per_fname, per_Iname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
		values
		(NULL, @ssn, @salt, 'Bobby', 'Sue', '123 Main St', 'Panama City Beach', 'FL', 324530221, 'bsue@fl.gov', '1962-05-16', 'j', 'new district judge');
		select 'show person data *after* adding person record' "as";
		select per_id, per_fname, per_Iname from person;
		do sleep(5);

		select 'show judge/judge_hist data *before* AFTER INSERT trigger fires (trg_judge_history_after_insert)' " as ";
		select * from judge;
		select * from judge_hist;
		do sleep(7);
		-- dsfaf-fdsfsdafs

		DROP TRIGGER IF EXISTS trg_judge_history_after_insert;
		DELIMITER //
		CREATE TRIGGER trg_judge_history_after_insert
		AFTER INSERT ON judge
		FOR EACH ROW
		BEGIN
		-- Note: concatenated data that goes into jhs_notes: modifying user *and* notes from judge table
		INSERT INTO judge_hist
		(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
		VALUES
		(
		NEW.per_id, NEW.crt_id, current_timestamp(), 'i', NEW.jud_salary,
		concat("modifying user: ", user(), " Notes: ", NEW.jud_notes)
		);
		END //
		DELIMITER ;
		select 'fire trigger by inserting record into judge table' "as ";
		Do sleep(5);
		INSERT INTO judge
		(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
		values
		-- or use 16 for per)id
		((select count(per_id) from person ), 3, 175000, 31, 'transfered from neighboring jurisdiction');
 		-- select max(per_id) from person where per_type="j" -- more robust, but the script needs to be modified

		select 'show judge/judge_hist data *after* AFTER INSERT trigger fires (trg_judge_history_after_insert)' "as";
		select * from judge;
		select * from judge_hist;
		do sleep (7);

-- 5.1:52:01

		select 'show judge/judge_hist data *before* AFTER INSERT trigger fires (trg_judge_history_after_insert') "as";
		select * from judge;
		select * from judge_hist;
		do sleep (7);

		DELIMITER //
		CREATE TRIGGER trg_judge_history_after_update
		AFTER UPDATE ON judge
		FOR EACH ROW
		BEGIN
		-- Note: concatenated data that goes into jhs_notes: same as AFTER INSERT trigger
		INSERT INTO judge_hist
		(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
		VALUES
		(
		NEW.per_id, NEW.ert_id, current_timestamp(), 'u', NEW.jud_salary,
		concat("modifying user: ", user(), " Notes: ", NEW.jud_notes)
		);
		END //
		DELIMITER ;
		select 'fire trigger by updating latest judge entry (salary and notes)' "as ";
		do sleep(5);
		UPDATE judge
		SET jud_salary=190000, jud_notes='senior justice - longest serving member'
		WHERE per_id=16;
		Select 'show judge/judge_hist data *after* AFTER UPDATE trigger fires (trg_judge_history_after_update)' "as ";
		Select * from judge;
		Select * from judge_hist;
		do sleep(7);
		DROP TRIGGER IF EXISTS trg_judge_history_after_update;
		
-- 6. 1:57:30

		-- NOTE: can create a new person, or use an existing person record, as per below.

		drop procedure if exists sp_add_judge_record;

		DELIMITER //
		CREATE PROCEDURE sp_add_judge_record()

		BEGIN
		INSERT INTO judge
		(per_id, crt_id, jud_salary, jud_yeers_in_practice, jud_notes)
		values
		(6, 1, 110000, 0, concat("New judge was former attorney. ", "Modifying event creator: ", current_user()));
		END //

		DELIMITER ;

		select '1) check event_scheduler' "as";
		SHOW VARIABLES LIKE 'event_ scheduler';
		do sleep(5);

		select '2) if not, turn it on...' "as";
		SET GLOBAL event_scheduler = ON;

		select '3) recheck event_scheduler' "as";
		SHOW VARIABLES LIKE 'event_scheduler';
		do sleep(5);
		select 'show judge/judge hist data *after* event fires (one time add judge)' "as";
		select * from judge;
		select * from jusdge_hist;
		do sleep(7);
		-- extra credit 2:18:43
		-- USE THIS ONE AS DEMO
		-- Demo: deletes 1 judge history every 2 seconds, beginning 5 seconds after creation, running fer 30 seconds
		DROP EVENT IF EXISTS remove_judge_history;
		-- temporarily redefine delimiter
		DELIMITER //
		CREATE EVENT IF NOT EXISTS remove_judge_history
		ON SCHEDULE
		EVERY 2 SECOND
		STARTS NOW() + INTERVAL 5 SECOND
		ENDS NOW() + INTERVAL 30 SECOND
		COMMENT 'deletes all judge histories'
		DO
		BEGIN
		DELETE FROM judge_hist ORDER BY jhs_id desc LIMIT 1;
		END //
		-- change delimiter back
		DELIMITER ;

		select 'show events from mjowett;' "as ";
		-- list events for database
		SHOW EVENTS FROM mjowett;
		do sleep(5);
		select 'show state of event scheduler: show processlist;' "as ";
		show processlist;
		do sleep(5);
		-- DROP EVENT IF EXISTS remove_judge history;
		-- DEMO PURPOSES *ONLY®*: create stored proc to display removal of judge history records from event above.
		-- Note: could also just invoke SELECT ° FROM JUDGE HIST; every two or three seconds manually.

		select 'Demo how judge_hist data is removed “after® above event fires (remove _judge_history)' "as ";

		-- http://www.mysaltutorial.org/stored-procedures-loop.aspx
		-- loop through judge history data
		DROP PROCEDURE IF EXISTS sp_loop_judge_history_during_event;
		DELIMITER $$
		CREATE PROCEDURE sp_loop_judge_history_during_event()
		BEGIN
		-- create variables .
		DECLARE v_counter INT;
		DECLARE v_max INT;
		SET v_counter = 0, v_max = 5;
		-- create loop
		WHILE v_counter < v_max DO
		select * from judge_hist;
		DO SLEEP(7);
		set v_counter=v_counter+1;
		END WHILE;
		END$$
		DELIMITER ;
		CALL sp_loop_judge_history_during_event();
		CALL sp_loop_judge_history_during_event();
		-- DROP PROCEDURE IF EXISTS sp_loop_judge_history_during_event;

