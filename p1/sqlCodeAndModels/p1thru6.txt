-- _________________________________________________________________________________________
-- |                                                                                         |
-- |QUESTIONS 1 THROUGH 6 INCLUDING EXTRA CREDIT                                             |
-- |_________________________________________________________________________________________|

-- 1.

		Drop VIEW if exists v_attorney_info;
		CREATE VIEW v_attorney_info AS

		select
		concat(per_Iname, ", ", per_fname) as name,
		concat(per_street, ", ", per_city, ", ", per_state, "", per_zip) as address,
		TIMESTAMPDIFF(year, per_dob, now()) as age,
		CONCAT('$', FORMAT(aty_hourly_rate, 2)) as hourly_rate,
		bar_name, spc_type
		from person
		natural join attorney
		natural join bar
		natural join specialty
		order by per_Iname;

		select 'display view v_attorney_info' "as";

		select * from v_attorney_info;
		drop VIEW if exists v_attorney_info;


-- 2. 1:29:00 

		drop procedure if exists sp_num_judges_born_by_month;
		DELIMITER //
		CREATE PROCEDURE sp_num_judges_born_by_month()
		BEGIN
		select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
		from person
		natural join judge
		group by month_name
		order by month;
		END //
		DELIMITER ;

		select 'calling sp_num_judges_born_by_month()' "as";

		call sp_num_judges_born_by_month();
		drop procedure if exists sp_num_judges_born_by_month;

-- 3. 1:31:16

		drop procedure if exists sp_cases_and_judges;
		DELIMITER //
		CREATE PROCEDURE sp_cases_and_judges()
		BEGIN
		select per_id, cse_id, cse_type, cse_description,
		concat(per_fname, "", per_Iname) as name,
		concat('(',substring(phn_num, 1, 3), ')', substring(phn_num, 4, 3), '-', substring(phn_num, 7, 4)) as judge_office_num,
		phn_type,
		jud_years_in_practice,
		cse_start_date,
		cse_end_date
		from person
		natural join judge
		natural join `case`
		natural join phone
		where per_type='j'
		order by per_Iname;

		END //
		DELIMITER ;

		select 'sp_cases_and_judges()' "as" ;
		CALL sp_cases_and_judges();
		drop procedure if exists sp_cases_and_judges;
		
-- 4. 1:38:00

		select '4) Create a trigger that automatically adds a record to the judge history table for every record **added** to the judge table.' "as";
		do sleep(5);

		-- Note: technically, a judge could already be in the person table, yet not added to the judge table. Or, here, add a new person.
		-- add 16th person (a judge), before adding person to judge table (salt and hash per_ssn, and store unique salt, per_salt)

		select 'show person data *before* adding person record' "as";
		select per_id, per_fname, per_Iname from person;
		do sleep(5);

		-- give person a unique randomized salt, then hash and salt SSN
		SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user
		SET @num=000000000; -- Note: already provided random SSN from 111111111 - 999999999, inclusive above
		SET @ssn=unhex(sha2(concat(@salt, @num), 512)); -- salt and hash person's SSN 000000000

		INSERT INTO person
		(per_id, per_ssn, per_salt, per_fname, per_Iname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
		values
		(NULL, @ssn, @salt, 'Bobby', 'Sue', '123 Main St', 'Panama City Beach', 'FL', 324530221, 'bsue@fl.gov', '1962-05-16', 'j', 'new district judge');
		select 'show person data *after* adding person record' "as";
		select per_id, per_fname, per_Iname from person;
		do sleep(5);

		select 'show judge/judge_hist data *before* AFTER INSERT trigger fires (trg_judge_history_after_insert)' " as ";
		select * from judge;
		select * from judge_hist;
		do sleep(7);
		-- dsfaf-fdsfsdafs

		DROP TRIGGER IF EXISTS trg_judge_history_after_insert;
		DELIMITER //
		CREATE TRIGGER trg_judge_history_after_insert
		AFTER INSERT ON judge
		FOR EACH ROW
		BEGIN
		-- Note: concatenated data that goes into jhs_notes: modifying user *and* notes from judge table
		INSERT INTO judge_hist
		(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
		VALUES
		(
		NEW.per_id, NEW.crt_id, current_timestamp(), 'i', NEW.jud_salary,
		concat("modifying user: ", user(), " Notes: ", NEW.jud_notes)
		);
		END //
		DELIMITER ;
		select 'fire trigger by inserting record into judge table' "as ";
		Do sleep(5);
		INSERT INTO judge
		(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
		values
		-- or use 16 for per)id
		((select count(per_id) from person ), 3, 175000, 31, 'transfered from neighboring jurisdiction');
 		-- select max(per_id) from person where per_type="j" -- more robust, but the script needs to be modified

		select 'show judge/judge_hist data *after* AFTER INSERT trigger fires (trg_judge_history_after_insert)' "as";
		select * from judge;
		select * from judge_hist;
		do sleep (7);

-- 5.1:52:01

		select 'show judge/judge_hist data *before* AFTER INSERT trigger fires (trg_judge_history_after_insert') "as";
		select * from judge;
		select * from judge_hist;
		do sleep (7);

		DELIMITER //
		CREATE TRIGGER trg_judge_history_after_update
		AFTER UPDATE ON judge
		FOR EACH ROW
		BEGIN
		-- Note: concatenated data that goes into jhs_notes: same as AFTER INSERT trigger
		INSERT INTO judge_hist
		(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
		VALUES
		(
		NEW.per_id, NEW.ert_id, current_timestamp(), 'u', NEW.jud_salary,
		concat("modifying user: ", user(), " Notes: ", NEW.jud_notes)
		);
		END //
		DELIMITER ;
		select 'fire trigger by updating latest judge entry (salary and notes)' "as ";
		do sleep(5);
		UPDATE judge
		SET jud_salary=190000, jud_notes='senior justice - longest serving member'
		WHERE per_id=16;
		Select 'show judge/judge_hist data *after* AFTER UPDATE trigger fires (trg_judge_history_after_update)' "as ";
		Select * from judge;
		Select * from judge_hist;
		do sleep(7);
		DROP TRIGGER IF EXISTS trg_judge_history_after_update;
		
-- 6. 1:57:30

		-- NOTE: can create a new person, or use an existing person record, as per below.

		drop procedure if exists sp_add_judge_record;

		DELIMITER //
		CREATE PROCEDURE sp_add_judge_record()

		BEGIN
		INSERT INTO judge
		(per_id, crt_id, jud_salary, jud_yeers_in_practice, jud_notes)
		values
		(6, 1, 110000, 0, concat("New judge was former attorney. ", "Modifying event creator: ", current_user()));
		END //

		DELIMITER ;

		select '1) check event_scheduler' "as";
		SHOW VARIABLES LIKE 'event_ scheduler';
		do sleep(5);

		select '2) if not, turn it on...' "as";
		SET GLOBAL event_scheduler = ON;

		select '3) recheck event_scheduler' "as";
		SHOW VARIABLES LIKE 'event_scheduler';
		do sleep(5);
		select 'show judge/judge hist data *after* event fires (one time add judge)' "as";
		select * from judge;
		select * from jusdge_hist;
		do sleep(7);
		-- extra credit 2:18:43
		-- USE THIS ONE AS DEMO
		-- Demo: deletes 1 judge history every 2 seconds, beginning 5 seconds after creation, running fer 30 seconds
		DROP EVENT IF EXISTS remove_judge_history;
		-- temporarily redefine delimiter
		DELIMITER //
		CREATE EVENT IF NOT EXISTS remove_judge_history
		ON SCHEDULE
		EVERY 2 SECOND
		STARTS NOW() + INTERVAL 5 SECOND
		ENDS NOW() + INTERVAL 30 SECOND
		COMMENT 'deletes all judge histories'
		DO
		BEGIN
		DELETE FROM judge_hist ORDER BY jhs_id desc LIMIT 1;
		END //
		-- change delimiter back
		DELIMITER ;

		select 'show events from mjowett;' "as ";
		-- list events for database
		SHOW EVENTS FROM mjowett;
		do sleep(5);
		select 'show state of event scheduler: show processlist;' "as ";
		show processlist;
		do sleep(5);
		-- DROP EVENT IF EXISTS remove_judge history;
		-- DEMO PURPOSES *ONLY®*: create stored proc to display removal of judge history records from event above.
		-- Note: could also just invoke SELECT ° FROM JUDGE HIST; every two or three seconds manually.

		select 'Demo how judge_hist data is removed “after® above event fires (remove _judge_history)' "as ";

		-- http://www.mysaltutorial.org/stored-procedures-loop.aspx
		-- loop through judge history data
		DROP PROCEDURE IF EXISTS sp_loop_judge_history_during_event;
		DELIMITER $$
		CREATE PROCEDURE sp_loop_judge_history_during_event()
		BEGIN
		-- create variables .
		DECLARE v_counter INT;
		DECLARE v_max INT;
		SET v_counter = 0, v_max = 5;
		-- create loop
		WHILE v_counter < v_max DO
		select * from judge_hist;
		DO SLEEP(7);
		set v_counter=v_counter+1;
		END WHILE;
		END$$
		DELIMITER ;
		CALL sp_loop_judge_history_during_event();
		CALL sp_loop_judge_history_during_event();
		-- DROP PROCEDURE IF EXISTS sp_loop_judge_history_during_event;

