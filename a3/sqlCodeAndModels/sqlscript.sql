SET DEFINE OFF;
DROP SEQUENCE seq_cus_id; 
Create sequence seq_cus_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

-- 83 116
drop table customer CASCADE CONSTRAINTS PURGE;
CREATE TABLE customer
( 
cus_id number not null, 
cus_fname varchar2(15) not null, 
cus_Iname varchar2(30) not null, 
cus_street varchar2(30) not null, 
cus_city varchar2(30) not null, 
cus_state char(2) not null, 
cus_zip number(9) not null, -- equivalent to number(9,0)
cus_phone number(10) not null,
cus_balance number(7,2), -- 9999999.99 
cus_notes varchar2(255), 
CONSTRAINT pk_customer PRIMARY KEY(cus_id)
);
-- 83 114
DROP SEQUENCE seq_com_id; -- for auto increment 
Create sequence seq_com_id 
start with 1
increment by 1
minvalue 1
maxvalue 10000; 


drop table commodity CASCADE CONSTRAINTS PURGE;
CREATE TABLE commodity
(
com_id number not null,
com_name varchar2(20),
com_price NUMBER(8,2) NOT NULL,
cus_notes varchar2(255),
CONSTRAINT pk_commodity PRIMARY KEY(com_id),
CONSTRAINT uq_com_name UNIQUE(com_name)
);
-- 181 215
DROP SEQUENCE seq_ord_id; -- for auto increment
create sequence seq_ord_id
start with 1 
increment by 1 
minvalue 1
maxvalue 10000; 


drop table "order" CASCADE CONSTRAINTS PURGE;
CREATE TABLE "order" 
(
ord_id number(4,0) not null, -- max value 9999 (permitting only integers, no decimals)
cus_id number,
com_id number,
ord_num_units number(5,0) NOT NULL, -- max value 99999 (permitting only integers, no decimals)
ord_total_cost number(8,2) NOT NULL,
ord_notes varchar2(255), 
CONSTRAINT pk_order PRIMARY KEY(ord_id),
CONSTRAINT fk_order_customer
FOREIGN KEY (cus_id)
REFERENCES customer(cus_id),
CONSTRAINT fk_order_commodity
FOREIGN KEY (com_id)
REFERENCES commodity(com_id),
CONSTRAINT check_unit CHECK(ord_num_units > 0),
CONSTRAINT check_total CHECK(ord_total_cost > 0) 
);

-- Oracle NEXTVAL function used to retrieve next value in sequence
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Bev', 'Davis', '12 Main St.', 'Detroit', 'MI', 47222, 3155516212, 'bev@aol.com', 18500.99, 'recently moved');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Stephanie', 'Taylor', '46 Elm St.', 'St. Louis', 'MO', 57872, 4181851212, 'staylor@comcast.net', 25.01, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Dom', 'Carter', '789 Orange Ave.', 'Los Angeles', 'CA', 48942, 3435551212, 'dcarter@wow.com', 380.99, 'returning customer');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'John', 'Silverhand', '857 William Rd.', 'Phoenix', 'AZ', 25218, 4505551812, 'jsilverhand@aol.com', NULL, NULL);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Sal', 'Victorson', '534 Race Way', 'Charleston', 'WV', 77845, 9025551272, 'svictorson@wow.com', 570.76, 'new customer'); 
commit; 

-- 201 241

-- Oracle NEXTVAL function used to retrieve next value in sequence 

INSERT INTO commodity VALUES (seq_com_id.nextval, 'DVD & Player', 109.00, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Cereal', 3.00, 'sugar free');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Scrabble', 29.00, 'original');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Licorice', 1.89, NULL);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Turns', 2.45, 'antacid');
commit;

INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 2, 50, 200, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 3, 30, 100, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 1, 6, 654, NULL);
INSERT INTO "order" VAlUES (seq_ord_id.nextval, 5, 4, 24, 972, NULL); 
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 5, 7, 300, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 2, 5, 15, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 3, 40, 57, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 1, 4, 300, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5, 4, 14, 770, NULL);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 5, 15, 883, NULL);
commit;

select * FROM customer;
select * from commodity;
select * from "order";