
# LIS 3781 Assignment 3

## Alexander Blanchette

### Assignment 3 Requirements:

*Screenshot of deliverables for Assignment 3*

![AMPPS Installation Screenshot](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/83e7f03285c7f5394f05ab03b9fc9ea8860e07eb/a3/img/A3Deliverables.PNG)

#### README.md file should include the following items:

> 1. Screenshot of *your* SQL code;
> 2. [Optional: SQL code for the required reports](sqlCodeAndModels/sqlscript.sql "Screenshot of *your* populated tables (w/in the Oracle environment)")
> 3. Screenshot of *your* populated tables (w/in the Oracle environment)
> 4. Bitbucket repo links: *Your* lis3781 Bitbucket repo link
> 
> 

#### Assignment Screenshots:

*Commodity SQL Code and Table*:

![Commodity SQL Code and Table](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/83e7f03285c7f5394f05ab03b9fc9ea8860e07eb/a3/img/CommoditySQLCodeandTable.PNG)

*Customer SQL Code and Table*:

![Customer SQL Code and Table](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/83e7f03285c7f5394f05ab03b9fc9ea8860e07eb/a3/img/CustomerSQLCodeandTable.PNG)

*Order SQL Code and Table*:

![Order SQL Code and Table](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/83e7f03285c7f5394f05ab03b9fc9ea8860e07eb/a3/img/orderSQLCodeandTable.PNG)
