### Alexander Blanchette
# LIS 3781 - Assignment 5 Requirements:
  - Expanding upon the high-volume home office supply company�s data tracking requirements, the CFO requests your services again to extend the data model�s functionality
  - The CFO needs to you to create a small data mart to test the capabilities of data warehousing analytics and business intelligence.
  - Board members also want this expansion to track data on store fronts across the nation:
    - Add the following tables 
      - Region
        - State
        - City
        - Store. 
  - See A5 Deliverables for more rulses and constraints that must be met for this assignment.

![Assignment 5 Overview](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/7aaf88251affe319cfd1a579eec66599c4295b0f/a5/img/A5Ddeliverable.JPG)

## ERD For Assignment 4

![Assignment 5 ERD](https://bitbucket.org/AlexanderBlanchette/lis3781/raw/7aaf88251affe319cfd1a579eec66599c4295b0f/a5/img/A5ERD.JPG)

> Below you will find a .txt file containing all of the SQL script for A5.
>

>[A4 SQL Script](a5/sqlCodeAndModels/A5SQL.txt)s
